﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIEmpleados.Migrations
{
    public partial class actualizacionNombreEntidadDomicilio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Empleados_Domicilio_DomicilioId",
                table: "Empleados");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Domicilio",
                table: "Domicilio");

            migrationBuilder.RenameTable(
                name: "Domicilio",
                newName: "Domicilios");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Domicilios",
                table: "Domicilios",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Empleados_Domicilios_DomicilioId",
                table: "Empleados",
                column: "DomicilioId",
                principalTable: "Domicilios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Empleados_Domicilios_DomicilioId",
                table: "Empleados");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Domicilios",
                table: "Domicilios");

            migrationBuilder.RenameTable(
                name: "Domicilios",
                newName: "Domicilio");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Domicilio",
                table: "Domicilio",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Empleados_Domicilio_DomicilioId",
                table: "Empleados",
                column: "DomicilioId",
                principalTable: "Domicilio",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
