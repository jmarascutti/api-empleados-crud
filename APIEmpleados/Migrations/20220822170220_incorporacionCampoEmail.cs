﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIEmpleados.Migrations
{
    public partial class incorporacionCampoEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Edad",
                table: "Empleados");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Empleados",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Empleados");

            migrationBuilder.AddColumn<string>(
                name: "Edad",
                table: "Empleados",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
