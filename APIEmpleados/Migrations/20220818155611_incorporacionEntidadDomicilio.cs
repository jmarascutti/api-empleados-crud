﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace APIEmpleados.Migrations
{
    public partial class incorporacionEntidadDomicilio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DomicilioId",
                table: "Empleados",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Domicilio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Calle = table.Column<string>(type: "text", nullable: false),
                    Interseccion1 = table.Column<string>(type: "text", nullable: false),
                    Interseccion2 = table.Column<string>(type: "text", nullable: true),
                    Numero = table.Column<string>(type: "text", nullable: false),
                    Provincia = table.Column<string>(type: "text", nullable: false),
                    Localidad = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domicilio", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Empleados_DomicilioId",
                table: "Empleados",
                column: "DomicilioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Empleados_Domicilio_DomicilioId",
                table: "Empleados",
                column: "DomicilioId",
                principalTable: "Domicilio",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Empleados_Domicilio_DomicilioId",
                table: "Empleados");

            migrationBuilder.DropTable(
                name: "Domicilio");

            migrationBuilder.DropIndex(
                name: "IX_Empleados_DomicilioId",
                table: "Empleados");

            migrationBuilder.DropColumn(
                name: "DomicilioId",
                table: "Empleados");
        }
    }
}
