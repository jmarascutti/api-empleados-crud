﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIEmpleados.Migrations
{
    public partial class incorporacionCampoDniEmpleado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Dni",
                table: "Empleados",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Dni",
                table: "Empleados");
        }
    }
}
