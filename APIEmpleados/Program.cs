using APIEmpleados.Data;
using APIEmpleados.Models;
using APIEmpleados.Validator;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using static APIEmpleados.Constantes;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// conexion con la bd
var connectionString = builder.Configuration.GetConnectionString("PostgreSQLConnection");
builder.Services.AddDbContext<OfficeDb>(options =>
options.UseNpgsql(connectionString)); 

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();


// Almacena un empleado
app.MapPost("/empleados/", async(Empleado empleado, OfficeDb db) =>
    {
        // validar que el empleado no exista previamente
        var dni = await db.Empleados.FirstOrDefaultAsync(empleadoBusqueda => empleadoBusqueda.Dni == empleado.Dni);

        // si el dni no existe en la bd, almaceno al empleado
        if (dni == null )
        {
            var empleadoValidator = new EmpleadoValidator();
            ValidationResult resultadoEmpleadoValidator = empleadoValidator.Validate(empleado);

            if (!resultadoEmpleadoValidator.IsValid)
            {
                foreach (var error in resultadoEmpleadoValidator.Errors) {
                    return Results.Problem(error.ToString());
                }
            }

            var domicilioValidator = new DomicilioValidator();
            ValidationResult resultadoDomicilioValidator = domicilioValidator.Validate(empleado.Domicilio);

            if (!resultadoDomicilioValidator.IsValid)
            {
                foreach (var error in resultadoDomicilioValidator.Errors)
                {
                    return Results.Problem(error.ToString());
                }
            }


            db.Empleados.Add(empleado);
            await db.SaveChangesAsync();
            return Results.Created($"/empleado/{empleado.Id}", empleado);
        } else {
            return Results.Problem(EMPLEADO_ALTA_EXISTENTE);
        }
});


// Obtiene un empleado dado su id
app.MapGet("/empleados/{id:int}", async (int id, OfficeDb db) =>
    {
        return await db.Empleados.Include(empleado => empleado.Domicilio).FirstOrDefaultAsync(empleado => empleado.Id == id)
            is not Empleado empleado
            ? Results.NotFound(EMPLEADO_INEXISTENTE)
            : Results.Ok(empleado);
    });


// Obtiene todos los empleados
app.MapGet("/empleados/", async (OfficeDb db) =>
{
    List<Empleado> empleados = await db.Empleados.Include(empleado => empleado.Domicilio).ToListAsync();

      if (empleados.Count == 0)
      {
        return Results.NotFound(EMPLEADOS_INEXISTENTES_BD);
      } else  {
        return Results.Ok(empleados);
      }
});


// Obtiene todos los empleados cuyo apellido sea igual al recibido por parámetro
app.MapGet("/empleados/apellido", async (string apellido, OfficeDb db) =>
{
    var empleados = await db.Empleados.Include(empleado => empleado.Domicilio).ToListAsync(); 

    return empleados = empleados.Where(e => e.Apellido.Contains(apellido)).ToList();
});


// Modifica un empleado dado su id
app.MapPut("/empleado/{id:int}", async (int id, Empleado empleado, OfficeDb db) =>
{

    Empleado empleadoBD = await db.Empleados.Include(empleado => empleado.Domicilio).FirstOrDefaultAsync(empleado => empleado.Id == id);

    if (empleadoBD == null)
    {
        return Results.NotFound(EMPLEADO_MODIFICACION_INEXISTENTE);
    } else {
        // TODO: validar campos
        empleadoBD.Dni = empleado.Dni;
        empleadoBD.Nombre = empleado.Nombre;
        empleadoBD.Apellido = empleado.Apellido;
        empleadoBD.Email = empleado.Email;
        empleadoBD.FechaDeNacimiento = empleado.FechaDeNacimiento;
        empleadoBD.Domicilio = empleado.Domicilio;

        await db.SaveChangesAsync();
        return Results.Ok(MODIFICACION_EXITOSA);
    }
});


// Elimina un empleado dado su id
app.MapDelete("/empleado/{id:int}", async (int id, OfficeDb db) =>
{
    var empleado = await db.Empleados.FindAsync(id);   

    if (empleado is not null)
    {
        db.Empleados.Remove(empleado);
        await db.SaveChangesAsync();
        return Results.Ok(BAJA_EXITOSA);
    } else {
        return Results.NotFound(EMPLEADO_BAJA_INEXISTENTE);
    }
});


// Elimina todos los empleados
app.MapDelete("/empleados/", async (OfficeDb db) =>
{
    var empleados = await db.Empleados.Include(empleado => empleado.Domicilio).ToListAsync();

    if (empleados.Count == 0)
    {
        return Results.NotFound(EMPLEADOS_INEXISTENTES_ELIMINACION);
    }
    else
    {
        db.Empleados.RemoveRange(empleados);
        db.SaveChanges();
        return Results.Ok(BAJA_EXITOSA_EMPLEADOS);
    }

});



app.Run();


