﻿using APIEmpleados.Models;
using Microsoft.EntityFrameworkCore;

namespace APIEmpleados.Data
{
    public class OfficeDb : DbContext
    {
        public OfficeDb(DbContextOptions<OfficeDb> options): base(options)
        {

        }

        //public DbSet<Empleado> Empleados => Set<Empleado>();
        //public DbSet<Domicilio> Domicilios => Set<Domicilio>();
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Domicilio> Domicilios { get; set; }

    }
}
