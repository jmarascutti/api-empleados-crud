﻿using APIEmpleados.Models;
using FluentValidation;
using static APIEmpleados.Constantes;

namespace APIEmpleados.Validator
{
    public class DomicilioValidator : AbstractValidator<Domicilio>
    {
        public DomicilioValidator()
        {

            RuleFor(domicilio => domicilio.Calle)
                .NotEmpty().WithMessage(CALLE_VACIA)
                .MaximumLength(40).WithMessage(CALLE_LONGITUD_MAXIMA)
                .MinimumLength(5).WithMessage(CALLE_LONGITUD_MINIMA);


            RuleFor(domicilio => domicilio.Interseccion1)
                .NotEmpty().WithMessage(INTERSECCION1_VACIA)
                .MaximumLength(40).WithMessage(INTERSECCION1_LONGITUD_MAXIMA)
                .MinimumLength(5).WithMessage(INTERSECCION1_LONGITUD_MINIMA);


            RuleFor(domicilio => domicilio.Interseccion2)
                 .MaximumLength(40).WithMessage(INTERSECCION2_LONGITUD_MAXIMA)
                 .MinimumLength(5).WithMessage(INTERSECCION2_LONGITUD_MINIMA);


            // TODO: Validar sólo números
            RuleFor(domicilio => domicilio.Numero)
                 .NotEmpty().WithMessage(NUMERO_VACIO)
                 .Must(dni => dni.ToString().Length > 7).WithMessage(NUMERO_LONGITUD_MAXIMA)
                 .Must(dni => dni.ToString().Length < 1).WithMessage(NUMERO_LONGITUD_MINIMA);


            RuleFor(domicilio => domicilio.Provincia)
                .NotEmpty().WithMessage(PROVINCIA_VACIA)
                .MaximumLength(25).WithMessage(PROVINCIA_LONGITUD_MAXIMA)
                .MinimumLength(5).WithMessage(PROVINCIA_LONGITUD_MINIMA);


            RuleFor(domicilio => domicilio.Localidad)
               .NotEmpty().WithMessage(LOCALIDAD_VACIA)
               .MaximumLength(25).WithMessage(LOCALIDAD_LONGITUD_MAXIMA)
               .MinimumLength(5).WithMessage(LOCALIDAD_LONGITUD_MINIMA);
        }

    }
}
