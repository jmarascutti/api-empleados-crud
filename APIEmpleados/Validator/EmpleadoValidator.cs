﻿using APIEmpleados.Models;
using FluentValidation;
using static APIEmpleados.Constantes;

namespace APIEmpleados.Validator
{
    public class EmpleadoValidator : AbstractValidator<Empleado>
    {
        public EmpleadoValidator()
        {

            // TODO: Validar sólo números
            RuleFor(empleado => empleado.Dni.ToString())
                .NotEmpty().WithMessage(DNI_VACIO)                
                .Must(dni => dni.ToString().Length == 8).WithMessage(DNI_LONGITUD_EXACTA);
            

            RuleFor(empleado => empleado.Nombre)
                .NotEmpty().WithMessage(NOMBRE_VACIO)
                .MaximumLength(40).WithMessage(NOMBRE_LONGITUD_MAXIMA)
                .MinimumLength(3).WithMessage(NOMBRE_LONGITUD_MINIMA);


            RuleFor(empleado => empleado.Apellido)
                .NotEmpty().WithMessage(APELLIDO_VACIO)
                .MaximumLength(40).WithMessage(APELLIDO_LONGITUD_MAXIMA)
                .MinimumLength(3).WithMessage(APELLIDO_LONGITUD_MINIMA);


            RuleFor(empleado => empleado.Email)
                .MaximumLength(45).WithMessage(MAIL_LONGITUD_MAXIMA)
                .MinimumLength(6).WithMessage(MAIL_LONGITUD_MINIMA)
                .EmailAddress().WithMessage(MAIL_FORMATO_INCORRECTO);
               

            // TODO: crear regla para validar máscara de fecha Ej: DD/MM/AAAA
            RuleFor(empleado => empleado.FechaDeNacimiento)
               .NotEmpty().WithMessage(FECHA_NACIMIENTO_VACIO);

        }

    }
}
