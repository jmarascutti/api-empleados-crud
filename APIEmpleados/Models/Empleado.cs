﻿using System.ComponentModel.DataAnnotations;
using static APIEmpleados.Constantes;

namespace APIEmpleados.Models
{
    public class Empleado 
    {
        public int Id { get; set; }

        public int Dni { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string? Email { get; set; }

      
        public DateTime FechaDeNacimiento { get; set; }

        public Domicilio Domicilio { get; set; }

    }
}
