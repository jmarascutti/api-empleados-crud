﻿using System.ComponentModel.DataAnnotations;
using static APIEmpleados.Constantes;

namespace APIEmpleados.Models
{
    public class Domicilio
    {
        public int Id { get; set; }
      
        public string Calle { get; set; }

        public string Interseccion1 { get; set; }

        public string? Interseccion2 { get; set; }

        public int Numero { get; set; }

        public string Provincia { get; set; }
      
        public string Localidad { get; set; }

    }
}
