﻿namespace APIEmpleados
{
    public class Constantes
    {
        // VALIDACIONES HTTP REQUESTS

        public const string ALTA_EXITOSA = "El empleado ha sido guardado exitosamente";

        public const string MODIFICACION_EXITOSA = "El empleado ha sido modificado exitosamente";

        public const string BAJA_EXITOSA = "El empleado ha sido eliminado exitosamente";

        public const string EMPLEADO_INEXISTENTE = "El empleado buscado no existe";

        public const string EMPLEADOS_INEXISTENTES_ELIMINACION = "No existen empleados a eliminar";

        public const string EMPLEADOS_INEXISTENTES_BD = "No existen empleados almacenados";

        public const string EMPLEADO_ALTA_EXISTENTE = "El empleado a guardar ya se encuentra almacenado";

        public const string EMPLEADO_MODIFICACION_INEXISTENTE = "El empleado a modificar no se encuentra almacenado";

        public const string EMPLEADO_BAJA_INEXISTENTE = "El empleado a eliminar no se encuentra almacenado";

        public const string BAJA_EXITOSA_EMPLEADOS = "Los empleados han sido eliminados exitosamente";

        public const string PARAMETRO_INCORRECTO_ID_EMPLEADO = "El id recibido no coincide con el id de la persona";

        public const string PARAMETRO_EMPLEADO_INCORRECTO = "";


        // VALIDACIONES EMPLEADO 

        public const string DNI_VACIO = "El dni del empleado no puede estar vacío";

        public const string DNI_SOLO_NUMEROS = "El dni solo permite números";

        public const string DNI_LONGITUD_MAXIMA = "El dni no puede tener mas de 8 caracteres";

        public const string DNI_LONGITUD_MINIMA = "El dni no puede tener menos de 6 caracteres";

        public const string DNI_LONGITUD_EXACTA = "El dni debe tener exactamente 8 caracteres";


        public const string NOMBRE_VACIO = "El nombre del empleado no puede estar vacío";

        public const string NOMBRE_LONGITUD_MAXIMA = "El nombre no puede tener mas de 40 caracteres";

        public const string NOMBRE_LONGITUD_MINIMA = "El nombre no puede tener menos de 3 caracteres";


        public const string APELLIDO_VACIO = "El apellido del empleado no puede estar vacio";

        public const string APELLIDO_LONGITUD_MAXIMA = "El apellido no puede tener mas de 40 caracteres";

        public const string APELLIDO_LONGITUD_MINIMA = "El apellido no puede tener menos de 3 caracteres";


        public const string MAIL_LONGITUD_MAXIMA = "El email no puede tener mas de 45 caracteres";

        public const string MAIL_LONGITUD_MINIMA = "El email no puede tener menos de 6 caracteres";

        public const string MAIL_FORMATO_INCORRECTO = "El email debe tener el formato correo@micorreo.com.ar";


        public const string FECHA_NACIMIENTO_VACIO = "La fecha de nacimiento no puede estar vacía";



        // VALIDACIONES DOMICILIO

        public const string DOMICILIO_VACIO = "El domicilio no puede estar vacío";

        public const string CALLE_VACIA = "La calle no puede estar vacía";

        public const string CALLE_LONGITUD_MAXIMA = "La calle no puede tener mas de 40 caracteres";

        public const string CALLE_LONGITUD_MINIMA = "La calle no puede tener menos de 5 caracteres";


        public const string INTERSECCION1_VACIA = "La intersección 1 no puede estar vacía";

        public const string INTERSECCION1_LONGITUD_MAXIMA = "La intersección 1 no puede tener mas de 40 caracteres";

        public const string INTERSECCION1_LONGITUD_MINIMA = "La intersección 1 no puede tener menos de 5 caracteres";

        public const string INTERSECCION2_LONGITUD_MAXIMA = "La intersección 2 no puede tener mas de 40 caracteres";

        public const string INTERSECCION2_LONGITUD_MINIMA = "La intersección 2 no puede tener menos de 5 caracteres";



        public const string NUMERO_VACIO = "El número no puede estar vacío";

        public const string NUMERO_LONGITUD_MAXIMA = "El número no puede tener mas de 7 caracteres";

        public const string NUMERO_LONGITUD_MINIMA = "El número no puede tener menos de 1 caracter";


        public const string PROVINCIA_VACIA = "La provincia no puede estar vacía";

        public const string PROVINCIA_LONGITUD_MAXIMA = "La provincia no puede tener mas de 25 caracteres";

        public const string PROVINCIA_LONGITUD_MINIMA = "La provincia no puede tener menos de 5 caracteres";


        public const string LOCALIDAD_VACIA = "La localidad no puede estar vacía";

        public const string LOCALIDAD_LONGITUD_MAXIMA = "La localidad no puede tener mas de 25 caracteres";

        public const string LOCALIDAD_LONGITUD_MINIMA = "La localidad no puede tener menos de 5 caracteres";


    }
}
